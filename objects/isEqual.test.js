/**
 * Описание задачи: Напишите функцию, которая поверхностно сравнивает два объекта.
 * Ожидаемый результат: True если объекты идентичны, false если объекты разные ({ a: 1, b: 1 }, { a: 1, b: 1 }) => true
 * Сложность задачи: 2 of 5
 * @param {Object} firstObj - объект с примитивами
 * @param {Object} secondObj - объект с примитивами
 * @returns {boolean}
 */

const isEqual = (firstObject, secondObject) => {
    //код писать тут
};

const data = { a: 1, b: 1 };
const data2 = { a: 1, b: 1 };
const data3 = { a: 1, b: 2 };
console.log(isEqual(data, data2)); // true
console.log(isEqual(data, data3)); // false

/**
 * Tests
 * */

describe('isEqual', () => {
    it('should return false', () => {
        const obj1 = { a: 1, b: 2, c: 3 };
        const obj2 = { a: 1, b: 2, c: 4 };
        expect(isEqual(obj1, obj2)).toBe(false);
    });
    it('should return true', () => {
        const obj1 = { a: 1, b: 2, c: 3 };
        const obj2 = { a: 1, b: 2, c: 3 };
        expect(isEqual(obj1, obj2)).toBe(true);
    });
    it('should be pure function', () => {
        const obj1 = { a: 1, b: 2, c: 3 };
        const obj2 = { a: 1, b: 2, c: 4 };
        isEqual(obj1, obj2)
        expect(obj1).equal({ a: 1, b: 2, c: 3 });
        expect(obj2).equal({ a: 1, b: 2, c: 4 });
    });
})
