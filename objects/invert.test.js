/**
 * Описание задачи: Напишите функцию, которая меняет местами ключ со значением и возвращает новый массив
 * Ожидаемый результат: ( { 'a': 1, 'b': 2, 'c': 1 }) => { '1': 'c', '2': 'b' }
 * Сложность задачи: 2 of 5
 * @param {{string: string | number}}} object
 * @returns {{string: string}} object
 */

const invert = (object) => {
    // Решение писать тут. Эта необязательная задача со звездочкой :)
};

const object = { 'a': 1, 'b': 2, 'c': 1 }

console.log(invert(object)) // => { '1': 'c', '2': 'b' }

/**
 * Tests
 * */

describe('invert', () => {
    it('should invert object', () => {
        const data = { 'a': 1, 'b': 2, 'c': 1 }
        expect(invert(data)).toEqual({ '1': 'c', '2': 'b' });
    });
    it('should be pure function', () => {
        const data = { 'a': 1, 'b': 2, 'c': 1 }
        invert(data);
        expect(data).toEqual({ '1': 'c', '2': 'b' });
    });
})