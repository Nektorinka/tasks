/**
 * Описание задачи: Напишите функцию, которая вызывает метод массива на заданный путь объекта.
 * Ожидаемый результат: ({ a: { b: [1, 2, 3] } }, 'a.b', splice, [1, 2]) => [2, 3]
 * Сложность задачи: 3 of 5
 * @param {Object} object
 * @param {String} path - путь в объекте
 * @param {String} func - метод массива для исполнения
 * @param {Array} [args] - список аргументов
 * @returns {?}
 */

const invoke = (object, path, func, args) => {
    // Решение писать тут. Эта необязательная задача со звездочкой :)
};

const data = { a: { b: [1, 2, 3] } }
invoke(data, 'a.b', 'pop', [1]);

console.log(data) // {"a": {"b": [1]}}



/**
 * Tests
 * */

describe('invoke', () => {
    it('should run splice method', () => {
        const data = { a: { b: [1, 2, 3] } }
        invoke(data, 'a.b', 'splice', [1, 2]);
        expect(data).toEqual({"a": {"b": [1]}}
        );
    });
    it('should run push method', () => {
        const data = { a: { b: [1, 2, 3] } }
        invoke(data, 'a.b', 'push', ['asd'])
        expect(data).toEqual( {"a": {"b": [1, 2, 3, "asd"]}});
    });

})