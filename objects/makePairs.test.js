/**
 * Описание задачи: Напишите функцию, которая возвращает вложенный массив вида `[[key, value], [key, value]]`.
 * Ожидаемый результат: ({ a: 1, b: 2 }) => [['a', 1], ['b', 2]]
 * Сложность задачи: 1 of 5
 * @param {Object} object - любой объект для трансформации
 * @returns {Array} - вложенный массив
 */

const makePairs = (object) => {
    // Решение писать тут
}

const data = { a: 1, b: 2, 123: 'string', obj: {}, array: [] };
console.log(makePairs(data)); // [['a', 1], ['b', 2]]


/**
 * Tests
 * */

describe('makePairs', () => {
    it('should return array in array', () => {
        const data = { a: 1, b: 2, c: 'asd'};
        expect(makePairs(data)).toEqual([['a', 1], ['b', 2], ['c', 'asd']]);
    });
})
