/**
 * Описание задачи: Напишите функцию, которая возвращает новый объект без указанных значений.
 * Ожидаемый результат: ({ a: 1, b: 2 }, 'b') => { a: 1 }
 * Сложность задачи: 2 of 5
 * @param {Object} object - любой объект
 * @param {?} args - список значений для удаления
 * @returns {Object} - новый объект без удаленных значений
 */

const without = (object, ...args) => {
    // Код писать тут
};

const data = { a: 1, b: 2, c: 3 };
console.log(without(data, 'b', 'c')); // { a: 1 }


/**
 * Tests
 * */

describe('without', () => {
    it('should delete props', () => {
        const data = { a: 1, b: 2, c: 3 };
        expect(without(data, 'b', 'c')).toEqual({ a: 1 });
    });

    it('should not delete props without args', () => {
        const data = { a: 1, b: 2, c: 3 };
        expect(without(data)).toEqual({ a: 1, b: 2, c: 3 });
    });

    it('should not delete props at initial object', () => {
        const data = { a: 1, b: 2, c: 3 };
        expect(without(data)).not.toBe(data)
    });
})
